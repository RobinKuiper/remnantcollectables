import React, { useEffect } from "react";

const NEW_DOMAIN = process.env.NODE_ENV === "development" ? "http://localhost:3000" : "https://www.remnant-tools.com";

const IFrame = () => {
  useEffect(() => {
    function onMessage(event) {
      if (event.origin !== NEW_DOMAIN) {
        return;
      }

      if (event.data === "getData") {
        // Get the data from localStorage
        const data = localStorage.getItem("data");

        // Send the data back to the parent window
        window.parent.postMessage(
          {
            message: "unlocks",
            data,
          },
          NEW_DOMAIN,
        );
      }

      if (event.data === "getBuilds") {
        // Get the builds from localStorage
        const data = localStorage.getItem("builds");

        // Send the data back to the parent window
        window.parent.postMessage(
          {
            message: "builds",
            data,
          },
          NEW_DOMAIN,
        );
      }

      if (event.data === "getCredentials") {
        // Get the data from localStorage
        const data = localStorage.getItem("google_oauth");

        // Send the data back to the parent window
        window.parent.postMessage(
          {
            message: "credentials",
            data,
          },
          NEW_DOMAIN,
        );
      }
    }

    window.addEventListener("message", onMessage);
  }, []);

  return <h1>Hi</h1>;
};

export default IFrame;

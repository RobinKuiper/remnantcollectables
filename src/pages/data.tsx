import React, { useEffect, useState } from "react";

const Data = () => {
  const [data, setData] = useState("");

  useEffect(() => {
    // Parent window (React app)
    window.addEventListener("message", event => {
      console.log("Received message on 3000");
      if (event.origin !== "http://localhost:8000") {
        return; // Ignore messages from domains other than the iframe
      }

      // Handle the data received from the iframe
      const localStorageDataFromIframe = event.data;
      console.log("Data received from iframe:", localStorageDataFromIframe);
    });
  }, []);

  const getStorageData = () => {
    const iframe = document.getElementById("iframe");
    // Send a message to the iframe to get the data
    iframe.contentWindow.postMessage("getLocalStorageData", "http://localhost:8000");
  };

  return (
    <div>
      <h1>Cross Domain LocalStorage Sharing</h1>
      <p>The data in the localstorage of the iframe will be displayed here.</p>
      <button onClick={getStorageData}>Get Data</button>
      <p>Data: {data}</p>
      <iframe id="iframe" src="http://localhost:8000/iframe" style={{ display: "none" }}></iframe>
    </div>
  );
};

export default Data;

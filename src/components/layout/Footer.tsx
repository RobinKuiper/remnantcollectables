import "./Footer.scss";
import React from "react";
import { StaticImage } from "gatsby-plugin-image";
import { BiLogoPatreon, BiLogoPaypal } from "react-icons/bi";
import { BsDiscord, BsLinkedin } from "react-icons/bs";
import { AiFillGitlab } from "react-icons/ai";
import { graphql, useStaticQuery } from "gatsby";

const Footer = () => {
  const data = useStaticQuery(graphql`
    {
      site {
        siteMetadata {
          gitlab
          discord
          website
          linkedIn
          patreon
          paypal
        }
      }
    }
  `);

  return (
    <div className="footer-container">
      <div className="flex-item left">
        <div className="copyright">
          <a href={data.site.siteMetadata.website} title="Personal website of Robin Kuiper" target="_blank">
            {/*<BsLink45Deg />*/}
            <StaticImage src="../../images/rklogo.png" alt="Robin Kuiper Logo" height={40} />
          </a>
        </div>
      </div>

      <div className="flex-item center">
        <a
          className="icon-link paypal"
          href={data.site.siteMetadata.paypal}
          target="_blank"
          title="Paypal.me for Robin Kuiper"
          data-tooltip-id="tooltip"
          data-tooltip-content="Help keep this project running!"
        >
          <BiLogoPaypal />
          <span>Paypal</span>
        </a>

        <a
          className="icon-link patreon"
          href={data.site.siteMetadata.patreon}
          target="_blank"
          title="Patreon link for Robin Kuiper"
          data-tooltip-id="tooltip"
          data-tooltip-content="Help keep this project running!"
        >
          <BiLogoPatreon />
          <span>Patreon</span>
        </a>
      </div>

      <div className="flex-item right">
        <div className="socials">
          <a href={data.site.siteMetadata.discord} title="Discord profile for Robin Kuiper" target="_blank">
            <BsDiscord />
          </a>
          <a href={data.site.siteMetadata.linkedIn} title="LinkedIn profile for Robin Kuiper" target="_blank">
            <BsLinkedin />
          </a>
          <a href={data.site.siteMetadata.gitlab} title="Gitlab profile for Robin Kuiper" target="_blank">
            <AiFillGitlab />
          </a>
        </div>
      </div>
    </div>
  );
};

export default Footer;

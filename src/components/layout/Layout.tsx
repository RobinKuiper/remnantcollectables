import "../../global.scss";
import "./Layout.scss";
import React, { useEffect, useRef } from "react";
import TopBar from "./TopBar";
import SettingsSidebar from "./SettingsSidebar";
import type { ToastOptions, UpdateOptions } from "react-toastify";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Tooltip } from "react-tooltip";
import Footer from "./Footer";
import { Script } from "gatsby";
import { toggleSidebar } from "../../features/settings/settingsSlice";
import { useAppDispatch, useAppSelector } from "../../hooks";
import type { RootState } from "../../store";
import { FaGoogleDrive } from "react-icons/fa";
import { setSaveCompleted, updateBuilds, updateUnlocks } from "../../features/data/dataSlice";
import { setLoggedIn } from "../../features/auth/authSlice";
import { GoogleOAuthProvider } from "@react-oauth/google";
import { TIME_BETWEEN_GOOGLE_SAVES } from "../../constants";

const TOAST_SETTINGS = {
  pending: {
    text: "Pending changes...",
  },
  saving: {
    text: "Saving...",
  },
  saved: {
    text: "Saved to Google Drive!",
    className: "success",
  },
  error: {
    text: "Something went wrong. Please try again later.",
    className: "error",
  },
};

const OLD_DOMAIN = process.env.NODE_ENV === "development" ? "http://localhost:8000" : "https://remnant.rkuiper.nl";

interface Props {
  children: React.ReactNode;
}

const Layout = ({ children }: Props) => {
  const iframe = useRef();
  const showSettingsSidebar = useAppSelector((state: RootState) => state.settings.showSidebar);
  const { saving, pending, saved, lastSave, error } = useAppSelector((state: RootState) => state.data);
  const pendingRef = useRef();
  const savingRef = useRef();
  const savedRef = useRef();
  const intervalRef = useRef<NodeJS.Timer | null>();
  const dispatch = useAppDispatch();

  // Retrieve data from remnant.rkuiper.nl
  useEffect(() => {
    if (!iframe || !iframe.current) return;

    // Don't do this if we have data already
    if (localStorage.getItem("data") && 
      localStorage.getItem("builds")
    ) {
      return;
    }

    window.addEventListener("message", event => {
      if (event.origin !== OLD_DOMAIN) {
        return; // Ignore messages from domains other than the iframe
      }

      // Handle the data received from the iframe
      const data = event.data;

      if (data.message === "unlocks") {
        localStorage.setItem("data", data.data ?? "[]");
        dispatch(updateUnlocks());
      }

      if (data.message === "builds") {
        localStorage.setItem("builds", data.data ?? "{}");
        dispatch(updateBuilds());
      }

      if (data.message === "credentials") {
        if (data.data) {
          localStorage.setItem("google_oauth", data.data ?? "{}");
          dispatch(setLoggedIn());
        }
      }
    });

    setTimeout(() => {
      if (!localStorage.getItem("data")) {
        // Send a message to the iframe to get the data
        iframe.current.contentWindow.postMessage("getData", OLD_DOMAIN);
      }

      if (!localStorage.getItem("builds")) {
        // Send a message to the iframe to get the data
        iframe.current.contentWindow.postMessage("getBuilds", OLD_DOMAIN);
      }

      if (!localStorage.getItem("google_oauth")) {
        // Send a message to the iframe to get the data
        iframe.current.contentWindow.postMessage("getCredentials", OLD_DOMAIN);
      }
    }, 2000);
  }, [iframe.current]);

  const handleClick = () => {
    if (showSettingsSidebar) {
      dispatch(toggleSidebar());
    }
  };

  const getToastContent = toastSettings => {
    let color;
    switch (toastSettings.className) {
      case "success":
        color = "green";
        break;
      case "error":
        color = "red";
        break;
      default:
        color = "white";
        break;
    }

    return (
      <div className={`google-saving-toast ${toastSettings.className ?? ""}`}>
        <FaGoogleDrive size="25px" color={color} />
        {toastSettings.text}
      </div>
    );
  };

  useEffect(() => {
    if (pending && !intervalRef.current) {
      intervalRef.current = setInterval(() => {
        const currentTime = new Date().getTime();
        const lastSaveTime = lastSave.getTime();
        const timeSinceLastSave = (currentTime - lastSaveTime) / 1000; // Convert to seconds

        const config: UpdateOptions = {
          toastId: "google-toast",
          render: getToastContent({
            text: (
              <div className="pending">
                Pending changes...<span>{parseInt(String(TIME_BETWEEN_GOOGLE_SAVES - timeSinceLastSave))}s.</span>
              </div>
            ),
          }),
        };

        if (!savingRef.current && !savedRef.current) {
          toast.update("google-toast", config);
        }
      }, 1000);
    } else if (intervalRef.current !== null) {
      clearInterval(intervalRef.current);
      intervalRef.current = null;
    }
  }, [pending]);

  const getToastContentSettings = prioritizePending => {
    if (error) {
      return TOAST_SETTINGS["error"];
    } else if (!prioritizePending && savedRef.current) {
      return TOAST_SETTINGS["saved"];
    } else if (!prioritizePending && savingRef.current) {
      return TOAST_SETTINGS["saving"];
    } else if (pendingRef.current) {
      return TOAST_SETTINGS["pending"];
    }
  };

  const updateToast = (prioritizePending: boolean = false) => {
    if (!saving && !saved && !pendingRef.current && !error) return;

    const config: ToastOptions = {
      // type: error ? toast.TYPE.ERROR : toast.TYPE.DEFAULT,
      position: toast.POSITION.BOTTOM_RIGHT,
      autoClose: !prioritizePending && (saved || error) ? 5000 : false,
      hideProgressBar: !(!prioritizePending && (saved || error)),
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progressStyle: { background: error ? "red" : "green" },
      toastId: "google-toast",
      onClose: onToastClose,
    };

    const contentSettings = getToastContentSettings(prioritizePending);

    const isToast = document.getElementById("google-toast");
    if (isToast) {
      const updateConfig: UpdateOptions = config;
      updateConfig.render = getToastContent(contentSettings);
      delete updateConfig.toastId;
      toast.update("google-toast", config);
    } else {
      toast(getToastContent(contentSettings), config);
    }
  };

  const onToastClose = () => {
    dispatch(setSaveCompleted());
    if (pendingRef.current) {
      updateToast(true);
    }
  };

  useEffect(() => {
    pendingRef.current = pending;
    savingRef.current = saving;
    savedRef.current = saved;
    updateToast();
  }, [saving, pending, saved, error]);

  return (
    <div className="layout-container">
      <Script
        async
        src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2044382203546332"
        crossOrigin="anonymous"
      />

      <TopBar />

      <div className="layout-content" onClick={handleClick}>
        {children}
      </div>

      <Footer />

      <GoogleOAuthProvider clientId={process.env.GATSBY_CLIENT_ID ?? ""}>
        <SettingsSidebar />
      </GoogleOAuthProvider>

      <ToastContainer theme="dark" />
      <Tooltip id="tooltip" />
      <iframe ref={iframe} id="iframe" src={`${OLD_DOMAIN}/iframe`} style={{ display: "none" }}></iframe>
    </div>
  );
};

export default Layout;

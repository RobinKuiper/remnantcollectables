import "./TopBar.scss";
import { Link, graphql, useStaticQuery } from "gatsby";
import React, { useState } from "react";
import { RiSettings3Line } from "react-icons/ri";
import { StaticImage } from "gatsby-plugin-image";
import GlobalSearch from "./GlobalSearch";
import { useAppDispatch, useAppSelector } from "../../hooks";
import type { RootState } from "../../store";
import { toggleSidebar } from "../../features/settings/settingsSlice";
import { BiLogoPatreon } from "react-icons/bi";

const TopBar = () => {
  const data = useStaticQuery(graphql`
    {
      site {
        siteMetadata {
          patreon
        }
      }
    }
  `);
  const [isOpen, setOpen] = useState(false);
  const url = typeof window !== "undefined" ? window.location.href : "";
  const showSidebar = useAppSelector((state: RootState) => state.settings.showSidebar);
  const dispatch = useAppDispatch();

  const toggleOpen = () => {
    setOpen(!isOpen);
  };

  return (
    <div className="top-bar-container">
      <div className="left flex-item">
        <div id="logo">
          <Link to="/">
            <StaticImage src="../../images/logo.webp" alt="Remnant 2 Logo" height={50} />
          </Link>
        </div>
      </div>

      <div className="center flex-item">
        <nav className={isOpen ? "open" : ""}>
          <Link to="/database/archetypes" className={url.includes("database") ? "active" : ""}>
            Database
          </Link>
          <Link to="/tracker/archetypes" className={url.includes("tracker") ? "active" : ""}>
            Tracker
          </Link>
          <Link to="/builds" className={url.includes("builds") ? "active" : ""}>
            Builds
          </Link>

          <div className="patreon">
            <a href={data.site.siteMetadata.patreon} target="_blank" title="Robin Kuiper's Patreon">
              <BiLogoPatreon size={20} />
              <span>Patreon</span>
            </a>
          </div>
        </nav>
      </div>

      <div className="right flex-item">
        <div className="patreon">
          <a
            href={data.site.siteMetadata.patreon}
            target="_blank"
            title="Robin Kuiper's Patreon"
            data-tooltip-id="tooltip"
            data-tooltip-content="Help keep this project running!"
          >
            <BiLogoPatreon size={20} />
            <span>Patreon</span>
          </a>
        </div>

        <div className="search">
          <GlobalSearch />
        </div>

        <button className={`settings ${showSidebar ? "active" : ""}`} onClick={() => dispatch(toggleSidebar())}>
          <RiSettings3Line size="30px" />
        </button>

        <div className={`hamburger ${isOpen ? "open" : ""}`} onClick={toggleOpen}>
          <span className="hamburger__top-bun" />
          <span className="hamburger__bottom-bun" />
        </div>
      </div>
    </div>
  );
};

export default TopBar;

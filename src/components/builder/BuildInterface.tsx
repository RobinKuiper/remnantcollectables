import "./BuildInterface.scss";
import React from "react";
import BuildItemBox from "./BuildItemBox";

const BuildInterface = ({ activeBuild, images, openModal }) => {
  const ITEM_BOXES = {
    topLeft: [
      {
        buildPath: "headpiece",
        filters: [{ category: "armor" }, { type: "headpiece" }],
      },
      {
        buildPath: "chest",
        filters: [{ category: "armor" }, { type: "chest" }],
      },
      {
        buildPath: "feet",
        filters: [{ category: "armor" }, { type: "feet" }],
      },
      {
        buildPath: "hands",
        filters: [{ category: "armor" }, { type: "hands" }],
      },
    ],
    fragments: [
      {
        buildPath: "relic.fragment1",
        filters: [
          {
            key: "category",
            value: "relicfragments",
          },
          {
            key: "externalId",
            value: activeBuild?.relic?.fragment2,
            not: true,
          },
          {
            key: "externalId",
            value: activeBuild?.relic?.fragment3,
            not: true,
          },
        ],
      },
      {
        buildPath: "relic.fragment2",
        filters: [
          {
            key: "category",
            value: "relicfragments",
          },
          {
            key: "externalId",
            value: activeBuild?.relic?.fragment1,
            not: true,
          },
          {
            key: "externalId",
            value: activeBuild?.relic?.fragment3,
            not: true,
          },
        ],
      },
      {
        buildPath: "relic.fragment3",
        filters: [
          {
            key: "category",
            value: "relicfragments",
          },
          {
            key: "externalId",
            value: activeBuild?.relic?.fragment1,
            not: true,
          },
          {
            key: "externalId",
            value: activeBuild?.relic?.fragment2,
            not: true,
          },
        ],
      },
    ],
    topRight: [
      {
        buildPath: "amulet",
        filters: [{ category: "amulets" }],
      },
      {
        buildPath: "ring1",
        filters: [
          {
            key: "category",
            value: "rings",
          },
          {
            key: "externalId",
            value: activeBuild?.ring2,
            not: true,
          },
          {
            key: "externalId",
            value: activeBuild?.ring3,
            not: true,
          },
          {
            key: "externalId",
            value: activeBuild?.ring4,
            not: true,
          },
        ],
      },
      {
        buildPath: "ring2",
        filters: [
          {
            key: "category",
            value: "rings",
          },
          {
            key: "externalId",
            value: activeBuild?.ring1,
            not: true,
          },
          {
            key: "externalId",
            value: activeBuild?.ring3,
            not: true,
          },
          {
            key: "externalId",
            value: activeBuild?.ring4,
            not: true,
          },
        ],
      },
      {
        buildPath: "ring3",
        filters: [
          {
            key: "category",
            value: "rings",
          },
          {
            key: "externalId",
            value: activeBuild?.ring2,
            not: true,
          },
          {
            key: "externalId",
            value: activeBuild?.ring1,
            not: true,
          },
          {
            key: "externalId",
            value: activeBuild?.ring4,
            not: true,
          },
        ],
      },
      {
        buildPath: "ring4",
        filters: [
          {
            key: "category",
            value: "rings",
          },
          {
            key: "externalId",
            value: activeBuild?.ring2,
            not: true,
          },
          {
            key: "externalId",
            value: activeBuild?.ring3,
            not: true,
          },
          {
            key: "externalId",
            value: activeBuild?.ring1,
            not: true,
          },
        ],
      },
    ],
  };

  return (
    <div className="build-interface-container">
      <div id="top">
        <div id="armor" className="item-category">
          {ITEM_BOXES.topLeft.map(box => (
            <BuildItemBox
              key={box.buildPath}
              openModal={openModal}
              build={activeBuild}
              images={images}
              buildPath={box.buildPath}
              filters={box.filters}
            />
          ))}

          <div className="main-box">
            <BuildItemBox
              openModal={openModal}
              build={activeBuild}
              images={images}
              buildPath={"relic.externalId"}
              filters={[{ category: "relics" }]}
            />

            <div className="sub-boxes">
              {ITEM_BOXES.fragments.map(box => (
                <BuildItemBox
                  key={box.buildPath}
                  openModal={openModal}
                  build={activeBuild}
                  images={images}
                  buildPath={box.buildPath}
                  filters={box.filters}
                />
              ))}
            </div>
          </div>
        </div>

        <div id="stats"></div>

        <div id="accessoires" className="item-category">
          {ITEM_BOXES.topRight.map(box => (
            <BuildItemBox
              key={box.buildPath}
              openModal={openModal}
              build={activeBuild}
              images={images}
              buildPath={box.buildPath}
              filters={box.filters}
            />
          ))}
        </div>
      </div>

      <div id="bottom">
        <div className="main-box">
          <BuildItemBox
            openModal={openModal}
            build={activeBuild}
            images={images}
            buildPath={"mainHand.externalId"}
            filters={[{ category: "weapons" }, { type: "Long Guns" }]}
          />

          <div className="sub-boxes">
            <BuildItemBox
              openModal={openModal}
              build={activeBuild}
              images={images}
              buildPath={"mainHand.mod"}
              filters={[
                {
                  key: "category",
                  value: "mods",
                },
                {
                  key: "externalId",
                  value: activeBuild?.melee?.mod,
                  not: true,
                },
                {
                  key: "externalId",
                  value: activeBuild?.offhand?.mod,
                  not: true,
                },
                {
                  key: "links",
                  value: null,
                },
              ]}
              disabled={!!(activeBuild?.mainHand && activeBuild?.mainHand.mod && activeBuild?.mainHand.mod !== "")}
            />
            <BuildItemBox
              openModal={openModal}
              build={activeBuild}
              images={images}
              buildPath={"mainHand.mutator"}
              filters={[
                {
                  key: "category",
                  value: "mutators",
                },
                {
                  key: "externalId",
                  value: activeBuild?.melee?.mutator,
                  not: true,
                },
                {
                  key: "externalId",
                  value: activeBuild?.offhand?.mutator,
                  not: true,
                },
              ]}
            />
          </div>
        </div>
        <div className="main-box">
          <BuildItemBox
            openModal={openModal}
            build={activeBuild}
            images={images}
            buildPath={"melee.externalId"}
            filters={[{ category: "weapons" }, { type: "Melee Weapons" }]}
          />

          <div className="sub-boxes">
            {activeBuild?.melee && activeBuild?.melee.mod && (
              <BuildItemBox
                openModal={openModal}
                build={activeBuild}
                images={images}
                buildPath={"melee.mod"}
                filters={[{ category: "mods" }]}
                disabled={true}
              />
            )}
            <BuildItemBox
              openModal={openModal}
              build={activeBuild}
              images={images}
              buildPath={"melee.mutator"}
              filters={[
                {
                  key: "category",
                  value: "mutators",
                },
                {
                  key: "externalId",
                  value: activeBuild?.mainHand?.mutator,
                  not: true,
                },
                {
                  key: "externalId",
                  value: activeBuild?.offhand?.mutator,
                  not: true,
                },
              ]}
            />
          </div>
        </div>
        <div className="main-box">
          <BuildItemBox
            openModal={openModal}
            build={activeBuild}
            images={images}
            buildPath={"offhand.externalId"}
            filters={[{ category: "weapons" }, { type: "Hand Guns" }]}
          />

          <div className="sub-boxes">
            <BuildItemBox
              openModal={openModal}
              build={activeBuild}
              images={images}
              buildPath={"offhand.mod"}
              filters={[
                {
                  key: "category",
                  value: "mods",
                },
                {
                  key: "externalId",
                  value: activeBuild?.mainHand?.mod,
                  not: true,
                },
                {
                  key: "externalId",
                  value: activeBuild?.offhand?.mod,
                  not: true,
                },
              ]}
              disabled={!!(activeBuild?.offhand && activeBuild?.offhand.mod && activeBuild?.offhand.mod !== "")}
            />
            <BuildItemBox
              openModal={openModal}
              build={activeBuild}
              images={images}
              buildPath={"offhand.mutator"}
              filters={[
                {
                  key: "category",
                  value: "mutators",
                },
                {
                  key: "externalId",
                  value: activeBuild?.melee?.mutator,
                  not: true,
                },
                {
                  key: "externalId",
                  value: activeBuild?.mainHand?.mutator,
                  not: true,
                },
              ]}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default BuildInterface;

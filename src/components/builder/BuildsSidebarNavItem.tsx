import { AiFillCopy, AiFillDelete, AiFillStar, AiOutlineStar } from "react-icons/ai";
import React from "react";
import type { Build } from "../../interface/Build";

interface Props {
  build: Build;
  activeBuildId: number;
  selectBuild: (id: number) => void;
  copyBuild: (build: Build) => void;
  deleteBuild: (build: Build) => void;
  toggleBuildFavorite: (id: number) => void;
}

const BuildsSidebarNavItem = ({
  activeBuildId,
  build,
  selectBuild,
  copyBuild,
  deleteBuild,
  toggleBuildFavorite,
}: Props) => {
  const { id, name, favorite } = build;

  return (
    <div key={id} className={`nav-item ${id === activeBuildId && "active"} ${favorite && "favorite"}`}>
      <button onClick={() => toggleBuildFavorite(id)}>
        {favorite ? <AiFillStar size={20} /> : <AiOutlineStar size={20} />}
      </button>

      <button className="build-select" onClick={() => selectBuild(id)}>
        {name}
      </button>

      <div className="actions">
        <button onClick={() => copyBuild(id)} data-tooltip-id="tooltip" data-tooltip-content="Copy">
          <AiFillCopy />
        </button>
        <button onClick={() => deleteBuild(id)} data-tooltip-id="tooltip" data-tooltip-content="Delete">
          <AiFillDelete />
        </button>
      </div>
    </div>
  );
};

export default BuildsSidebarNavItem;

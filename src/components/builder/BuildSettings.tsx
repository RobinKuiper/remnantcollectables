import "./BuildSettings.scss";
import React, {useEffect, useState} from "react";
import {AiFillLock, AiFillUnlock} from "react-icons/ai";
import type {Build} from "../../interface/Build";
import type {DraftFunction} from "use-immer";
import {copyObject} from "../../helpers";

interface Props {
  updateBuildValue: (buildPath: string | string[], value: any) => void
  activeBuild: Build;
  onlyUnlocked: boolean;
  toggleOnlyUnlocked: () => void;
}

const BuildSettings = ({ updateBuildValue, activeBuild, toggleOnlyUnlocked, onlyUnlocked}: Props) => {
  const [name, setName] = useState(activeBuild ? activeBuild.name : "");

  useEffect(() => {
    if (!activeBuild) return;

    setName(activeBuild.name);
  }, [activeBuild]);

  const handleNameSave = () => {
    if (!activeBuild) return;

    if (name && name !== "") {
      updateBuildValue("name", name);
    }
  };

  const handleNameChange = e => {
    setName(e.target.value);
  };

  return (
    <div className="build-settings-container">
      <input type="text" placeholder="Name" value={name} onChange={handleNameChange} onBlur={handleNameSave}/>
      <div>
        <button
          onClick={toggleOnlyUnlocked}
          data-tooltip-id="tooltip"
          data-tooltip-content={onlyUnlocked ? "Showing only unlocked items" : "Showing all items"}
          data-tooltip-place="bottom"
        >
          {onlyUnlocked ? <AiFillUnlock size={"30px"}/> : <AiFillLock size={"30px"}/>}
        </button>
      </div>
    </div>
  );
};

export default BuildSettings;

import "./BuildsSidebarContent.scss";
import React, { useEffect, useState } from "react";
import type { Build } from "../../interface/Build";
import BuildsSidebarNavItem from "./BuildsSidebarNavItem";
import {DraftFunction} from "use-immer";

interface Props {
  builds: {
    [key: number]: Build;
  };
  setActiveBuildId: (arg: (DraftFunction<number> | number)) => void;
  resetBuild: () => void;
  activeBuildId: number;
  copyBuild: (build: Build) => void;
  deleteBuild: (build: Build) => void;
  toggleBuildFavorite: (id: number) => void;
}

const BuildsSidebarContent = ({
  builds,
  setActiveBuildId,
  resetBuild,
  activeBuildId,
  copyBuild,
  deleteBuild,
  toggleBuildFavorite,
}: Props) => {
  const [allBuilds, setAllBuilds] = useState([]);
  const [favoriteAmount, setFavoriteAmount] = useState(0);

  useEffect(() => {
    const b = Object.values(builds);
    setAllBuilds(b.map(build => build));
    setFavoriteAmount(b.filter(build => build.favorite).length)
  }, [builds]);

  const selectBuild = (id: number) => {
    localStorage.setItem("activeBuildId", id.toString());
    setActiveBuildId(id);
  };

  return (
    <div className="builds-sidebar-content-container">
        <div className="favorites">
          <div className="title">Favorites</div>
          <nav>
            {favoriteAmount ? allBuilds.map(b => (
              <BuildsSidebarNavItem
                key={`favorite_${b.id}`}
                activeBuildId={activeBuildId}
                build={b}
                selectBuild={selectBuild}
                copyBuild={() => copyBuild(b.id)}
                deleteBuild={() => deleteBuild(b.id)}
                toggleBuildFavorite={toggleBuildFavorite}
              />
            )) : (
              <div className="no-data">No favorites yet.</div>
            )}
          </nav>
        </div>

      <div className="builds">
        <div className="title">Saved builds</div>
        <nav>
          {allBuilds.length ? (
            Object.values(builds).map(b => (
              <BuildsSidebarNavItem
                key={b.id}
                activeBuildId={activeBuildId}
                build={b}
                selectBuild={selectBuild}
                copyBuild={() => copyBuild(b.id)}
                deleteBuild={() => deleteBuild(b.id)}
                toggleBuildFavorite={toggleBuildFavorite}
              />
            ))
          ) : (
            <div className="no-data">No saved builds found.</div>
          )}
        </nav>
      </div>

      <button className="new-build-button" onClick={resetBuild}>
        New Build
      </button>
    </div>
  );
};

export default BuildsSidebarContent;

import "./ArchetypesInterface.scss";
import React, { useEffect, useState } from "react";
import BuildItemBox from "./BuildItemBox";
import type { Build } from "../../interface/Build";
import type { Filter } from "../../interface/IData";
import ItemLevelNew from "../database/ItemLevelNew";
import { Link, graphql, useStaticQuery } from "gatsby";
import { getFieldValue } from "../../dataHelpers";

interface Props {
  activeBuild: Build;
  openModal: (filters: Filter[], buildPath: string) => void;
  images: any[];
  handleLevelChange: (level: number, buildPath: string) => void;
}

const ArchetypesInterface = ({ activeBuild, openModal, images, handleLevelChange }: Props) => {
  const { items } = useStaticQuery(graphql`
    {
      items: allItem(filter: { category: { in: ["traits", "archetypes"] } }) {
        nodes {
          externalId
          name
          fragment
        }
      }
    }
  `);
  const [archetype1, setArchetyp1] = useState();
  const [archetype2, setArchetyp2] = useState();
  const [trait1, setTrait1] = useState();
  const [trait2, setTrait2] = useState();

  useEffect(() => {
    [1, 2].forEach(index => {
      const externalId = getFieldValue(activeBuild, `archetype${index}.externalId`);
      if (externalId) {
        const archetype = items.nodes.find(item => item.externalId === externalId);
        if (index === 1) setArchetyp1(archetype);
        else if (index === 2) setArchetyp2(archetype);
      }

      const traitEId = getFieldValue(activeBuild, `archetype${index}.trait`);
      if (traitEId) {
        const trait = items.nodes.find(item => item.externalId === traitEId);
        if (index === 1) setTrait1(trait);
        else if (index === 2) setTrait2(trait);
      }
    });
  }, [activeBuild]);

  return (
    <div className="archetypes-interface-container">
      <div className="archetype one">
        <h3>{archetype1?.name ?? "Pick archetype"}</h3>
        <BuildItemBox
          openModal={openModal}
          build={activeBuild}
          images={images}
          buildPath={"archetype1.externalId"}
          filters={[
            {
              key: "category",
              value: "archetypes",
            },
            {
              key: "externalId",
              value: activeBuild?.archetype2?.externalId,
              not: true,
            },
          ]}
        />
        <strong>
          <Link to={`/database/traits/${trait1?.fragment}`} title={trait1?.name}>
            {trait1?.name}
          </Link>
        </strong>
        <ItemLevelNew
          level={activeBuild?.archetype1?.level}
          callback={(level: number) => handleLevelChange(level, "archetype1.level")}
          maxLevel={10}
          disabled={!activeBuild?.archetype1}
        />
      </div>

      <div className="archetype two">
        <h3>{archetype2?.name ?? "Pick archetype"}</h3>
        <BuildItemBox
          openModal={openModal}
          build={activeBuild}
          images={images}
          buildPath={"archetype2.externalId"}
          filters={[
            {
              key: "category",
              value: "archetypes",
            },
            {
              key: "externalId",
              value: activeBuild?.archetype1?.externalId,
              not: true,
            },
          ]}
        />
        <strong>
          <Link to={`/database/traits/${trait2?.fragment}`} title={trait2?.name}>
            {trait2?.name}
          </Link>
        </strong>
        <ItemLevelNew
          level={activeBuild?.archetype2?.level}
          callback={(level: number) => handleLevelChange(level, "archetype2.level")}
          maxLevel={10}
          disabled={!activeBuild?.archetype2}
        />
      </div>
    </div>
  );
};

export default ArchetypesInterface;

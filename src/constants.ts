export const VERSION = "SITE_VERSION";
export const LAST_UPDATED = "CURRENT_DATE";
export const SITE_TITLE = "Remnant 2 Tools";
export const MAX_TRAIT_POINTS = 65;
export const TIME_BETWEEN_GOOGLE_SAVES = process.env.NODE_ENV === "development" ? 20 : 60;

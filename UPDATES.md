# Update Log

[![Latest Release](https://gitlab.com/RobinKuiper/remnantcollectables/-/badges/release.svg)](https://gitlab.com/RobinKuiper/remnantcollectables/-/releases)

## Next

- Added the ability to import your save file!
- Improved the styling of the tool, including the top bar and sidebars.
- Improved a lot of the backend code, so everything should run smoother.
- Mobile layout improvements.
- Fixed a bug that caused the sidebar to be too tall on mobile devices.
- Made other minor bug fixes and improvements.

## v1.2.x

Mainly bug fixes and some data improvements.

https://gitlab.com/RobinKuiper/remnantcollectables/-/releases/v1.2.3
https://gitlab.com/RobinKuiper/remnantcollectables/-/releases/v1.2.2
https://gitlab.com/RobinKuiper/remnantcollectables/-/releases/v1.2.1

## v1.2.0

- Added a way to link your Google account and save the unlocks data to Google Drive
- Added a global search feature in the top bar to search through all data (with keyboard navigation)
- Added trait search on the [builder](https://www.remnant-tools.com/builds) (traits) page
- Added a way to unlock an item from the single item page
- Added a "Saved" indicator to the top bar
- Removed the annoying "Saved" toast
- Added tooltips to multiple buttons
- Improved mobile layout for different pages
- Improved ui performance (everything should feel smoother now)

#### Data

- Added the Labyrinth Set to the armor sets data
- Fixed a bug where going to the [Alpha/Omega](https://www.remnant-tools.com/database/weapons/alpha_omega) item page it would return Not Found

## Version 1.0.5

- Added a sidebar to the [builder](https://www.remnant-tools.com/builds) with statistics
- Improved design for the [builder](https://www.remnant-tools.com/builds)
- Improved design for the single item pages
- Added tooltips for items
- Added loading indicators to different parts of the site
- Improved list/grid view system
- Removed default view setting from settings
- And yet another complete data overhaul
- Codebase/build improvements

## Version 1.0.4

- Added [archetype](https://www.remnant-tools.com/database/archetypes) selection to [builder](https://www.remnant-tools.com/builds)
- Added [Mantagora](https://www.remnant-tools.com/database/bosses/mantagora) boss
- Linking more items between each other
- Fixed [statistic](https://www.remnant-tools.com/tracker/statistics) count bugs
- Removed [armorsets](https://www.remnant-tools.com/database/armorset) from [statistics](https://www.remnant-tools.com/tracker/statistics) lists
- Complete [builder](https://www.remnant-tools.com/builds) codebase overhaul

## Version 1.0.3

- Added more bosses (aberrations)
- Simple item pages per item
- Links in the statistic panels
- Tooltips for the different setting sidebar options and buttons
- Moved `Venom` from `Yaesha` to `Root Earth`
- Improved image system
- Codebase improvements

## Version 1.0.2

- Added a setting in the settings sidebar to toggle default showing of redacted items
- Save build as 'New Build' if no name is given
- Weapon specific mods are now not shown in the tracker anymore
- Scrolling in sidebars on smaller screens
- Added a 404 page
- Codebase fixes
- Layout fixes

## Version 1.0.1

- Added a settings sidebar
- Added export/import features to the settings sidebar
- A lot of codebase improvements
- Fixed a bug where tooltips could render below other elements

## Version 1.0.0

- Image improvements in the tracker and database lists
- Added default weapon mods to the database
- Added more data to weapons and armors
- Added more armor (sets)
- Added more missing images
- Added more relic fragments
- Added armor statistics to the builder
- Added traits to the builder

## Version 0.9.0

- Data improvements
- List and grid view
- Mobile ready
- Group by filter
